#!/usr/bin/env bash
cd /usr/local/ansible-scripts/
git reset --hard && git clean -fd
git pull
bash /usr/local/ansible-scripts/nginx/ansible-steps.sh