#!/usr/bin/python3
# -*- coding: utf-8 -*-

import psycopg2


conn = psycopg2.connect(dbname='nginx_upstream', user='postgres', host='localhost')


ansible_nginx_task_file = '/usr/local/ansible-scripts/nginx/linux_server/tasks/nginx_test.yml'


# Очищаем файл ansible_nginx_task_file
open(ansible_nginx_task_file, 'w').close()





def send_nginx_data():

    # Open a cursor to perform database operations
    with conn.cursor() as cur:
        

        # Create task to fetch all data from servers       
        # query = "SELECT linux_server_name, upstream_config_path FROM upstream_settings WHERE upstream_config_state = 'changed'"
        query = str("SELECT linux_server_name, upstream_config_path, upstream_config_state FROM upstream_settings")
        cur.execute(query)


        print (cur)
        ansible_nginx_tasks = open(ansible_nginx_task_file, "a")
        for user_data in (cur):
            print (user_data[2])
            if user_data[2] == "changed":
                
                # Create task to check nginx config
                ansible_nginx_tasks.write("- name: Check nginx config on server " + user_data[0] + "\n")      
                ansible_nginx_tasks.write("  shell:" + "\n")
                ansible_nginx_tasks.write("    cmd: if nginx -t 2>&1 | grep 'syntax is ok' > /dev/null; then echo 0; else echo 1; fi" + "\n") 
                ansible_nginx_tasks.write("  register: nginx_config_check_status" + "\n")
                ansible_nginx_tasks.write("  tags: test_nginx_configs" + "\n")      
                ansible_nginx_tasks.write("  changed_when: False" + "\n" )
                ansible_nginx_tasks.write("\n"+ "\n")


                # Create task to restore old config
                ansible_nginx_tasks.write("- name: restore old config on server " + (user_data[0]) + " with upstream config " + (user_data[1]) + "\n")
                ansible_nginx_tasks.write("  copy: " + "\n")
                ansible_nginx_tasks.write("    src: " + user_data[1] + ".bak" "\n")
                ansible_nginx_tasks.write("    dest: " + user_data[1] + "\n")
                ansible_nginx_tasks.write("    remote_src: yes" + "\n")
                ansible_nginx_tasks.write("  when: nginx_config_check_status.stdout == \"1\" and inventory_hostname in ['" +  user_data[0] + "']" + "\n")
                ansible_nginx_tasks.write("  tags: test_nginx_configs" + "\n")
                ansible_nginx_tasks.write("\n"+ "\n")


        ansible_nginx_tasks.close()
        # Make the changes to the database persistent
        conn.commit()
        
    return ()

def nginx_reload():

    # Open a cursor to perform database operations
    with conn.cursor() as cur:
        

        # Create task to fetch all data from servers       
        query = str("SELECT linux_server_name FROM upstream_settings WHERE upstream_config_state='changed'")
        cur.execute(query)


        print (cur)
        cur = (set(cur))
        ansible_nginx_tasks = open(ansible_nginx_task_file, "a")
        for user_data in (cur):
            
            # Create task to reload nginx 
            ansible_nginx_tasks.write("- name: Restart nginx on server " + user_data[0] + "\n")      
            ansible_nginx_tasks.write("  shell:" + "\n")
            ansible_nginx_tasks.write("    cmd: /etc/init.d/nginx reload" + "\n") 
            ansible_nginx_tasks.write("  when: nginx_config_check_status.stdout == \"0\" and inventory_hostname in ['" + user_data[0] + "']" + "\n")
            ansible_nginx_tasks.write("  tags: test_nginx_configs" + "\n")
            ansible_nginx_tasks.write("  run_once: true" + "\n")
            ansible_nginx_tasks.write("\n"+ "\n")

        ansible_nginx_tasks.close()
        # Make the changes to the database persistent
        conn.commit()

    return ()





send_nginx_data()
nginx_reload()