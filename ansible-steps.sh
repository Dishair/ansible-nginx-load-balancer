#!/usr/bin/env bash

# Create tasks to get configs
printf '1 ########### Create tasks to get configs ########### \n'
python3 /usr/local/ansible-scripts/nginx/1_get_nginx_data.py

# Get nginx configs to ansible server
printf '2 ########### Get nginx configs to ansible server ########### \n'
ansible-playbook /usr/local/ansible-scripts/nginx/site_roles.yml --tags "get_nginx_configs"

# Replace old upstream data to new
printf '3 ########### Replace old upstream data to new ########### \n'
python3 /usr/local/ansible-scripts/nginx/2_get_sql_data.py

# Create tasks to send configs
printf '4 ########### Create tasks to send configs ########### \n'
python3 /usr/local/ansible-scripts/nginx/3_send_nginx_data.py

# Send configs to remote hosts
printf '5 ########### Send configs to remote hosts ########### \n'
ansible-playbook /usr/local/ansible-scripts/nginx/site_roles.yml --tags "send_nginx_configs"

# Create tasks to test nginx on remote hosts
printf '6 ########### Create tasks to test nginx on remote hosts ########### \n'
python3 /usr/local/ansible-scripts/nginx/4_test_nginx.py

# test nginx configs on remote hosts
printf '7 ########### test nginx configs on remote hosts ########### \n'
ansible-playbook /usr/local/ansible-scripts/nginx/site_roles.yml --tags "test_nginx_configs"
