The main purpose is the input and output of nodes from the balance.

A set of python scripts that generate tasks for ansible-playbook in yaml format based on data from the database and run on the specified hosts.

Consists of the following steps:
1) Create tasks to get configs
2) Get nginx configs to ansible server
3) Replace old upstream data to new
4) Create tasks to send configs
5) Send configs to remote hosts
6) Create tasks to test nginx on remote hosts
7) test nginx configs on remote hosts
