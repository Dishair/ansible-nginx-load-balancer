#!/usr/bin/python3
# -*- coding: utf-8 -*-

import psycopg2
import re
# import secrets
# import string


conn = psycopg2.connect(dbname='nginx_upstream', user='postgres', host='localhost')


vars_file = '/usr/local/ansible-scripts/nginx/linux_server/vars/main.yml'

tasks_file = '/usr/local/ansible-scripts/nginx/linux_server/tasks/main.yml'



ansible_vars = open(vars_file, "a")

ansible_tasks = open(tasks_file, "a")


def upstream_config_state_insert(upstream_config_state, upstream_config_path):
    with conn.cursor() as cur:
        query=("UPDATE upstream_settings SET upstream_config_state='" + upstream_config_state + "' WHERE upstream_config_path='" + upstream_config_path + "'")
        cur.execute(query)
        conn.commit()        
    return ()


def get_sql_data():

    # Open a cursor to perform database operations
    with conn.cursor() as cur:
        # Change nginx upstream settings
        query = str('SELECT "linux_server_name", "upstream_config_path", "upstream_name", "upstream_enabled_servers", \
            "upstream_disabled_servers", "upstream_parameters" FROM upstream_settings')
        cur.execute(query)
        x = 0
        for user_data in cur:
            x = x + 1
            print ('\n')
            print (x)

            upstream_enabled_servers = user_data[3]
            upstream_enabled_servers = re.sub(',', ';\n    server ', str(upstream_enabled_servers), count=0, flags=re.MULTILINE)
            upstream_enabled_servers = "\n    server " + upstream_enabled_servers + ";"
            upstream_disabled_servers = user_data[4]
            upstream_disabled_servers = re.sub(',', ';\n#    server ', str(upstream_disabled_servers), count=0, flags=re.MULTILINE)
            upstream_disabled_servers = "\n#    server " + upstream_disabled_servers + ";"
            upstream_config = str(user_data[5])
            if(len(upstream_config) == 0) or upstream_config == 'None':
                upstream_config = "# upstream config is clear"
            else:
                None
            new_upstream = "\n" + "\n" + "  " + "upstream " + str(user_data[2]) + " {\n" + "    " + upstream_config + upstream_enabled_servers \
                + upstream_disabled_servers + "\n}"
            local_config_path = "/tmp/" + user_data[0] + user_data[1]
            config_open = open(local_config_path, "r")
            # search_string = r'.*upstream\s+' + user_data[2] + r'.*\{.*\n(?:.+\n)*(?:\s*server\s.+\n)+.*\}.*'
            old_upstream = user_data[2]
            search_string = r'\s*upstream\s+' + old_upstream + r'(?:\s|\n)*{(?:.|\n)*}'

            ### TESTS ###
            # config_path = "/usr/local/ansible-scripts/nginx/upstream.conf"
            # config_open = open(config_path, "r")
            # search_string = r'.*upstream\s+' + "z-kafka-upstream" + r'.*\{.*\n(?:.+\n)*(?:\s*server\s.+\n)+.*\}.*'
            ### TESTS ###
            
            # Удаляем лишние данные из параметров апстрима
            match = re.search(search_string, config_open.read())            
            if match:
                new_match = re.sub(r'\}(?:.|\n)*', r'}', match[0], count=0)
                old_upstream = new_match

            else:
                print ("string not found")



            # Replace old upstream settings to new
            print (old_upstream)
            print ("\n")
            print (new_upstream)

            if new_upstream != old_upstream:
                with open(local_config_path, 'r') as file:
                    data = file.read()
                    data = data.replace(old_upstream, new_upstream)

                with open(local_config_path, 'w') as config:
                    config.write(data)
                    config.close()
                upstream_config_state_insert("changed", user_data[1])
            else:
                upstream_config_state_insert("not_changed", user_data[1])
                print ("config change skipped")

        # Make the changes to the database persistent
        conn.commit()
    # ansible_tasks.write("---" + "\n" + " # tasks for work with nginx" + "\n" + "\n" + "\n")   
        
    return ()

get_sql_data()