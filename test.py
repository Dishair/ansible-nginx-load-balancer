#!/usr/bin/python3
# -*- coding: utf-8 -*-


import re

config_file = '/usr/local/ansible-scripts/nginx/upstream.conf'

config_open = open(config_file, "r")

# print (config_open.read())


match = re.findall(r'.+\{.*\n(?:.+\n)*(?:\s*server\s.+\n)+.*\}.*', config_open.read())

# print (match)

x = 1
for upstream in match:
    # print (str(x) + " upstream")
    x = x + 1
    upstream = re.split(r'\n', upstream)
    for string in upstream:
        match = re.findall(r'\s*upstream\s.+', string)
        if match:
            print ("upstream name")
            print (string)
        match = re.findall(r'\s*upstream\s.+|\s*server\s.+|\s*\}\s*', string)
        if not match:
            print ("config")
            print (string)
        match = re.findall(r'\s*server\s.+|^[}]', string)
        if match:
            print ("servers")
            print (string)
    print ("\n")