#!/usr/bin/python3
# -*- coding: utf-8 -*-

import psycopg2
import re


conn = psycopg2.connect(dbname='nginx_upstream', user='postgres', host='localhost')


vars_file = '/usr/local/ansible-scripts/nginx/linux_server/vars/main.yml'

tasks_file = '/usr/local/ansible-scripts/nginx/linux_server/tasks/main.yml'


# Очищаем файл vars
open(vars_file, 'w').close()


ansible_vars = open(vars_file, "a")

ansible_tasks = open(tasks_file, "a")



def send_nginx_data():

    # Open a cursor to perform database operations
    with conn.cursor() as cur:
        

        # Create task to fetch all data from servers       
        ansible_tasks.write("\n" + "\n")
        query = str("SELECT linux_server_name, upstream_config_path FROM upstream_settings WHERE upstream_config_state='changed'")
        cur.execute(query)
        
        for user_data in set(cur):
            
            ansible_tasks.write("- name: Send file from ansible to server " + (user_data[0]) + " with upstream config " + (user_data[1]) + "\n")      
            ansible_tasks.write("  copy: " + "\n")
            ansible_tasks.write(r"    src: /tmp/{{ inventory_hostname }}" + user_data[1] + "\n") 
            ansible_tasks.write("    dest: " + user_data[1]+ "\n")      
            # ansible_tasks.write("  changed_when: False" + "\n")
            ansible_tasks.write("  when: inventory_hostname in ['" +  user_data[0] + "']" + "\n")
            ansible_tasks.write("  tags: send_nginx_configs" + "\n")
            ansible_tasks.write("\n"+ "\n")
        ansible_tasks.write("- include: nginx_test.yml" + "\n")
                  
        ansible_tasks.close()
        # Make the changes to the database persistent
        conn.commit()
        
    return ()


send_nginx_data()
