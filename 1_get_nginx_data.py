#!/usr/bin/python3
# -*- coding: utf-8 -*-

import psycopg2
import re
# import secrets
# import string


conn = psycopg2.connect(dbname='nginx_upstream', user='postgres', host='localhost')


vars_file = '/usr/local/ansible-scripts/nginx/linux_server/vars/main.yml'

tasks_file = '/usr/local/ansible-scripts/nginx/linux_server/tasks/main.yml'


# Очищаем файл vars
open(vars_file, 'w').close()

# Очищаем файл tasks
open(tasks_file, 'w').close()



ansible_vars = open(vars_file, "a")

ansible_tasks = open(tasks_file, "a")






def get_nginx_data():

    # Open a cursor to perform database operations
    with conn.cursor() as cur:
        

        # Create task to fetch all data from servers       
        ansible_tasks.write("---" + "\n" + "# tasks file for nginx" + "\n" + "\n" + "\n")
        query = str('SELECT "linux_server_name", "upstream_config_path" FROM upstream_settings')
        cur.execute(query)
        
        for user_data in set(cur):
            
            ansible_tasks.write("- name: Get file from server " + (user_data[0]) + " with upstream config " + (user_data[1]) + "\n")      
            ansible_tasks.write("  fetch: " + "\n")
            ansible_tasks.write("    src: " + user_data[1]+ "\n") 
            ansible_tasks.write(r"    dest: /tmp/{{ inventory_hostname }}" + user_data[1] + "\n")      
            ansible_tasks.write("    flat: true" + "\n")
            # ansible_tasks.write("  changed_when: False" + "\n")
            ansible_tasks.write("  when: inventory_hostname in ['" +  user_data[0] + "']" + "\n")
            ansible_tasks.write("  tags: get_nginx_configs" + "\n")
            ansible_tasks.write("\n"+ "\n")
            
            
            ansible_tasks.write("- name: Backup config on server " + (user_data[0]) + " with upstream config " + (user_data[1]) + "\n")
            ansible_tasks.write("  copy: " + "\n")
            ansible_tasks.write("    src: " + user_data[1]+ "\n")
            ansible_tasks.write("    dest: " + user_data[1]+ ".bak" + "\n")
            ansible_tasks.write("    remote_src: yes" + "\n")
            # ansible_tasks.write("  changed_when: False" + "\n")
            ansible_tasks.write("  when: inventory_hostname in ['" +  user_data[0] + "']" + "\n")
            ansible_tasks.write("  tags: get_nginx_configs" + "\n")
            ansible_tasks.write("\n"+ "\n")

        ansible_tasks.close()
        # Make the changes to the database persistent
        conn.commit()
        
    return ()


get_nginx_data()
